#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time : 2021/6/28 10:06
# @Author : ZhongYeHai
# @Site : 
# @File : fileView.py
# @Software: PyCharm

import os
import time

from flask import request, send_from_directory

from config.config import conf
from .. import api
from ...utils import restful
from ...utils.globalVariable import FILE_ADDRESS
from ...baseView import BaseMethodView
from ...utils.required import login_required


def format_time(atime):
    """ 时间戳转年月日时分秒 """
    return time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(atime))


def make_pagination(data_list, pag_size, page_num):
    """ 数据列表分页 """
    start = (page_num - 1) * pag_size
    end = start + pag_size
    return data_list[start: end]


@api.route('/file/list')
@login_required
def get_file_list():
    """ 文件列表 """
    pag_size = request.args.get('pageSize') or conf['page']['pageSize']
    page_num = request.args.get('pageNum') or conf['page']['pageNum']
    file_list = os.listdir(FILE_ADDRESS)

    # 分页
    filter_list = make_pagination(file_list, int(pag_size), int(page_num))

    parsed_file_list = []
    for file_name in filter_list:
        file_info = os.stat(os.path.join(FILE_ADDRESS, file_name))
        parsed_file_list.append({
            'name': file_name,  # 文件名
            'size': file_info.st_size,  # 文件文件大小
            'lastVisitTime': format_time(file_info.st_atime),  # 最近一次使用时间
            'LastModifiedTime': format_time(file_info.st_mtime),  # 最后一次更新时间
        })
    return restful.success('获取成功', data={'data': parsed_file_list, 'total': file_list.__len__()})


@api.route('/file/check', methods=['GET'])
@login_required
def check_file():
    """ 检查文件是否已存在 """
    file_name = request.args.get('name')
    return restful.fail(f'文件 {file_name} 已存在') if os.path.exists(
        os.path.join(FILE_ADDRESS, file_name)) else restful.success('文件不存在')


@api.route('/file/download', methods=['GET'])
@login_required
def download_file():
    """ 下载文件 """
    return send_from_directory(FILE_ADDRESS, request.args.to_dict().get('name'), as_attachment=True)


@api.route('/upload', methods=['POST'], strict_slashes=False)
# @login_required
def file_upload():
    """ 文件上传 """
    file, skip = request.files['file'], request.form.get('skip')  # 是否覆盖已存在名字的文件
    if os.path.exists(os.path.join(FILE_ADDRESS, file.filename)) and skip != '1':
        return restful.fail(msg='文件已存在，请修改文件名字后再上传', data=file.filename)
    else:
        file.save(os.path.join(FILE_ADDRESS, file.filename))
        return restful.success(msg='上传成功', data=file.filename)


class FileManage(BaseMethodView):
    """ 文件管理视图 """

    decorators = []

    def get(self):
        filename = request.args.to_dict().get('name')
        return send_from_directory(FILE_ADDRESS, filename, as_attachment=True)

    def post(self):
        """ 上传文件 """
        file_name_list = []
        for file_io in request.files.getlist('files'):
            file_io.save(os.path.join(FILE_ADDRESS, file_io.filename))
            file_name_list.append(file_io.filename)
        return restful.success(msg='上传成功', data=file_name_list)

    def delete(self):
        """ 删除文件 """
        name = request.get_json(silent=True).get('name')
        path = os.path.join(FILE_ADDRESS, name)
        if os.path.exists(path):
            os.remove(path)
        return restful.success('删除成功', data={'name': name})


api.add_url_rule('/file', view_func=FileManage.as_view('file'))
